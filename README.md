Wireguard
=========

Installs and configures [Wireguard](https://www.wireguard.com/) server Debian/Ubuntu servers.

Requirements
----------------
[netaddr](https://pypi.org/project/netaddr/)
`pip install netaddr`

Example Playbook
----------------
```yml
- hosts: all
  become: true
  roles:
    - role: tyumentsev4.wireguard
      vars:
        wireguard_subnet: 10.0.0.1/24
        wireguard_clients:
          - android
          - windows
```
